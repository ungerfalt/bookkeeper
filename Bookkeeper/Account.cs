﻿using System;
using SQLite;

namespace Bookkeeper
{
	public class Account
	{
		[PrimaryKey, AutoIncrement]
		public int Number{ get; set; }
		public string Name{ get; set; }

	
		public Account(){
		}

		public override string ToString ()
		{
			return Name + " " + Number;
		}

	}
}

