﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using System.Linq;

namespace Bookkeeper
{
	public class BookAdapter : BaseAdapter
	{
		private Activity activity;
		private List<Entry> entries;
		public BookAdapter (Activity activity, List<Entry> entries)

		{
			this.activity = activity;
			this.entries = entries;
			
		}
			

		public override Java.Lang.Object GetItem (int position)
		{
			throw new NotImplementedException ();
		}

		public override long GetItemId (int position)
		{
			return position;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			if (convertView == null) {
				convertView = activity.LayoutInflater.Inflate(Resource.Layout.EntryListItem,parent,false);
			}
			TextView text1 = convertView.FindViewById<TextView> (Resource.Id.textView1);
			TextView text2 = convertView.FindViewById<TextView> (Resource.Id.textView2);
			TextView text3 = convertView.FindViewById<TextView> (Resource.Id.textView3);


			Entry entry = entries [position];

			text1.Text = "" + entry.Rate;
			text2.Text = "" + entry.Bankaccount;
			text3.Text = "" + entry.Amount +"kr";



			return convertView;


		}

		public override int Count {
			get {
				return entries.Count;
			}
		}

	}
}

