﻿using System;
using System.Collections.Generic;
using SQLite;
using System.Linq;

namespace Bookkeeper
{
	public class BookkeeperManager



	{
		//private List<Entry> entries;
		private List<Account> bankAccounts;
		private List<Account> incomeAccounts;
		private List<Account> expenseAccounts;
		private List<TaxRate> taxes;
		private Entry amount;



		private static BookkeeperManager instance = null;
		public static BookkeeperManager Instance {
			get {
				if (instance == null) {
					instance = new BookkeeperManager ();
				}
				return instance;
			}
		}


	



	

		public List<Account> BankAccounts
		{
			get
			{
				return bankAccounts;
			}
		}
		public List<Account> IncomeAccounts
		{
			get
			{
				return incomeAccounts;
			}
		}
		public List<Account> ExpenseAccounts
		{
			get
			{
				return expenseAccounts;
			}
		}

		public List<TaxRate> Taxes
		{
			get
			{
				return taxes;
			}
		}
		public Entry Amount
		{
			get
			{
				return amount;
			}
	
		}

		private SQLiteConnection db;
			

		private BookkeeperManager() 
		{

			string dbPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
			db = new SQLiteConnection (dbPath + @"\myDataBase.db");

			incomeAccounts = new List<Account> ();
			expenseAccounts = new List<Account> ();
			bankAccounts = new List<Account> ();
			taxes = new List<TaxRate> ();
			Entry entry = new Entry ();
			entry.Amount = entry.Amount * entry.Rate + entry.Amount;
		


			try{
				db.Table<Entry>().Count();

			}catch(SQLiteException e){
				Console.WriteLine (e);
				db.CreateTable<Entry> ();
				}

			bankAccounts.Add (new Account (){ Name = "Personkonto", Number = 780709 });
			bankAccounts.Add (new Account (){ Name = "Sparkonto", Number = 298209 });
			bankAccounts.Add (new Account (){ Name = "Resekonto", Number = 4912 });

			incomeAccounts.Add (new Account () { Name = "Bagageloppis", Number = 12345 });
			incomeAccounts.Add (new Account () { Name = "Daytrading", Number = 666111 });
			incomeAccounts.Add (new Account () { Name = "Sommarjobb", Number = 2341234 });

			expenseAccounts.Add (new Account () { Name = "Elektronikprylar", Number = 90987 });
			expenseAccounts.Add (new Account () { Name = "Resor", Number = 345667 });
			expenseAccounts.Add (new Account () { Name = "Fest", Number = 763278 });

			taxes.Add (new TaxRate () { Taxrate = 0.06f });
			taxes.Add (new TaxRate () { Taxrate = 0.12f });
			taxes.Add (new TaxRate () { Taxrate = 0.25f });



		try{
				db.Table<Account>().Count();
			}catch(Exception e){
				Console.WriteLine (e);
				db.CreateTable<Account> ();
		}

			try{
				db.Table<TaxRate>().Count();
			}catch(Exception e){
				Console.WriteLine (e);
				db.CreateTable<TaxRate> ();
			}


			if (db.Table<Account> ().Count () < 0 && db.Table<TaxRate>().Count() < 0)   {


				foreach (TaxRate a in Taxes) {
					db.Insert (a);
				}
				foreach (Account a in BankAccounts) {
					db.Insert (a);
				}
				foreach (Account a in IncomeAccounts) {
					db.Insert (a);
				}
			
				foreach (Account a in ExpenseAccounts) {
					db.Insert (a);
				}
			}

		




	








		}


		public void AddEntry(Entry e)
		{
			db.Insert (e);
		}

		public List<Entry> Entries{
			get{
				return db.Table<Entry> ().ToList ();
			}
		}

		public List<Entry> GetList(){

			IEnumerable<Entry> qry = db.Table<Entry> ();
			List<Entry> list = qry.ToList();

			return list;
		}
	



	}
}

