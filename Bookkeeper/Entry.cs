﻿using System;
using SQLite;

namespace Bookkeeper
{
	
	public class Entry
	{
		
		[PrimaryKey, AutoIncrement]
		public int Id{ get; set;}
		public string Bankaccount{get; set;}
		public string Income{get; set;}
		public string Expense{get; set;}
		public float Rate{get; set;} 
		public string Description{get; set;}
		public string Date{ get; set;}
		public double Amount{ get; set; }

		public override string ToString ()
		{
			return "" + Amount * Rate;
		}

	

	} 


}

