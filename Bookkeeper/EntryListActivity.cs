﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookkeeper
{
	[Activity (Label = "EntryListActivity")]			
	public class EntryListActivity : Activity
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.EntryList);

			BookkeeperManager bookManager = BookkeeperManager.Instance;

			List<Entry> entries = bookManager.Entries;

			BookAdapter adapter = new BookAdapter (this, entries);

			ListView listView = FindViewById<ListView> (Resource.Id.listView1);
			listView.Adapter = adapter;
		}
	}
}

