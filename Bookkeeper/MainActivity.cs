﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace Bookkeeper
{
	[Activity (Label = "Bookkeeper", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			Button newEntryButton = FindViewById<Button> (Resource.Id.button1);
			Button showEntryButton = FindViewById<Button> (Resource.Id.button2);
			
			newEntryButton.Click += delegate {
				StartActivity(typeof(New_Entry));
			};
			showEntryButton.Click += delegate {
				StartActivity(typeof(EntryListActivity));
			};
		}
	}
}


