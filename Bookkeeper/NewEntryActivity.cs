﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookkeeper
{
	[Activity (Label = "New_Entry")]			
	public class New_Entry : Activity
	{
		private EditText editAmount;
		private List<Account> moneyAccounts;
		private List<Account> expenseAccounts;
		private List<Account> incomeAccounts;
		private BookkeeperManager bookManager;
		private Button entryButton;
		private Spinner moneyAccount;
		private Spinner incomeAccount;
		private Spinner expenseAccount;
		private Spinner taxrate;
		private EditText editDate;
		private EditText descriptionText;
		private EditText totalCost;

	



		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.NewEntry);

			RadioButton incomeButton = FindViewById<RadioButton> (Resource.Id.radioButtonIncome);
			RadioButton expenseButton = FindViewById<RadioButton> (Resource.Id.radioButtonExpense);
			entryButton = FindViewById<Button> (Resource.Id.entry_button);
			editDate = FindViewById<EditText> (Resource.Id.editDate);
			descriptionText = FindViewById<EditText> (Resource.Id.descriptionText);
			totalCost = FindViewById<EditText> (Resource.Id.editText_amount);



			bookManager = BookkeeperManager.Instance;
			BookkeeperManager tax = BookkeeperManager.Instance;
			List<Account> moneyAccounts = bookManager.BankAccounts;
			List<Account> incomeAccounts = bookManager.IncomeAccounts;
			List<Account> expenseAccounts = bookManager.ExpenseAccounts;


			List<TaxRate> taxes = bookManager.Taxes;

		
			Checked (false, true);
		    Checked (true, false);

			moneyAccount = FindViewById<Spinner> (Resource.Id.moneyAccountSpinner);
			taxrate = FindViewById<Spinner> (Resource.Id.taxRateSpinner);



			ArrayAdapter adapter = new ArrayAdapter<Account>(this, Android.Resource.Layout.SimpleSpinnerItem, moneyAccounts);
			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			moneyAccount.Adapter = adapter;

			ArrayAdapter adapter3 = new ArrayAdapter<TaxRate>(this, Android.Resource.Layout.SimpleSpinnerItem, taxes);
			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			taxrate.Adapter = adapter3;
		
			incomeButton.Click += delegate {
				Checked(true,false);

			};

			expenseButton.Click += delegate {
				Checked(false,true);

			};

			entryButton.Click += (object sender, EventArgs e) => {
				Entry entry = new Entry();
				entry.Date = editDate.Text;
				entry.Bankaccount = ((ArrayAdapter<Account>)moneyAccount.Adapter).GetItem (moneyAccount.SelectedItemPosition).Name;
				entry.Income = ((ArrayAdapter<Account>) incomeAccount.Adapter).GetItem (incomeAccount.SelectedItemPosition).Name;
				entry.Expense = ((ArrayAdapter<Account>) expenseAccount.Adapter).GetItem (expenseAccount.SelectedItemPosition).Name;
				entry.Rate = ((ArrayAdapter<TaxRate>) taxrate.Adapter).GetItem (taxrate.SelectedItemPosition).Taxrate;

				entry.Description = descriptionText.Text;
				entry.Amount = Convert.ToDouble(totalCost.Text);

				bookManager.AddEntry(entry);
			
				};
		}

		public void Checked(bool income, bool expense)
		{
			if (income == true) {
				incomeAccounts = bookManager.IncomeAccounts;
				incomeAccount = FindViewById<Spinner> (Resource.Id.accountTypeSpinner);
				ArrayAdapter adapter2 = new ArrayAdapter<Account>(this, Android.Resource.Layout.SimpleSpinnerItem, incomeAccounts);
				adapter2.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
				incomeAccount.Adapter = adapter2;
			}

			else if (expense == true) {
			    expenseAccounts = bookManager.ExpenseAccounts;
				expenseAccount = FindViewById<Spinner> (Resource.Id.accountTypeSpinner);
				ArrayAdapter adapter4 = new ArrayAdapter<Account> (this, Android.Resource.Layout.SimpleSpinnerItem, expenseAccounts);
				adapter4.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
				expenseAccount.Adapter = adapter4;
			}
				

		}


	}
}

