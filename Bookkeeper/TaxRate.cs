﻿using System;
using SQLite;

namespace Bookkeeper
{
	public class TaxRate
	{
		[PrimaryKey, AutoIncrement]
		public int Id{ get; set;}
		public float Taxrate {get; set;}

		public TaxRate (float tax)
		{
			Taxrate = tax;
		}
		public TaxRate()
		{
		
		}
		public override string ToString ()
		{
			return "" + Taxrate * 100 + "%";
		}


	}
}

